package com.parttime.parttime;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

/**
 * Created by almantera on 06/04/18.
 */

public class CategoryActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout llTeacher, llSecurity, llWriter, llDevelop;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        llTeacher = (LinearLayout) findViewById(R.id.kategori_teacher);
        llSecurity = (LinearLayout) findViewById(R.id.kategori_security);
        llWriter = (LinearLayout) findViewById(R.id.kategori_writer);
        llDevelop = (LinearLayout) findViewById(R.id.kategori_develop);
        llTeacher.setOnClickListener(this);
        llSecurity.setOnClickListener(this);
        llWriter.setOnClickListener(this);
        llDevelop.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.kategori_teacher:
                Toast.makeText(this, "Clicked teacher category",
                        Toast.LENGTH_SHORT).show();
                break;
            case R.id.kategori_security:
                Toast.makeText(this, "Clicked security category",
                        Toast.LENGTH_SHORT).show();
                break;
            case R.id.kategori_writer:
                Toast.makeText(this, "Clicked writer category",
                        Toast.LENGTH_SHORT).show();
                break;
            case R.id.kategori_develop:
                Toast.makeText(this, "Clicked developer category",
                        Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
